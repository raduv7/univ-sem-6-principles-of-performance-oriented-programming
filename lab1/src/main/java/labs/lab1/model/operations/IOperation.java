package labs.lab1.model.operations;

public interface IOperation {
    public Double calculate();
}
